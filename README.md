# autoi3
A couple of helper scripts for i3

### close_ws.py
Closes everything on the focused workspace

### new_ws.py
Opens up a new workspace after the current focused workspace

### insert_ws.py
Moves all following workspaces forward and creates a new workspace, after the focused workspace

### next_ws.py
Switches to the next workspace on the focused monitor

### prev_ws.py
Switches to the previous workspace on the focused monitor

### mv_next_ws.py
Moves container to the next workspace on the focused monitor

### mv_prev_ws.py
Moves container to the previous workspace on the focused monitor


### next_visible_ws.py
Switches to the next visible workspace (a.k.a. "switch focus to next monitor")

### Example i3 config
```
# Switch workspaces
bindsym $mod+Prior exec --no-startup-id ~/Development/autoi3/next_ws.py
bindsym $mod+Next exec --no-startup-id ~/Development/autoi3/prev_ws.py

# Move containers
bindsym $mod+Shift+Prior exec --no-startup-id ~/Development/autoi3/mv_next_ws.py
bindsym $mod+Shift+Next exec --no-startup-id ~/Development/autoi3/mv_prev_ws.py

# New workspace
bindsym $mod+Home exec --no-startup-id ~/Development/autoi3/new_ws.py

# Insert new workspace
bindsym $mod+Insert exec --no-startup-id ~/Development/autoi3/insert_ws.py

# Switch monitor by switching to the next visible workspace
bindsym $mod+End exec --no-startup-id ~/Development/autoi3/next_visible_ws.py

# Close entire workspace
bindsym $mod+Shift+Delete exec --no-startup-id ~/Development/autoi3/close_ws.py
```

